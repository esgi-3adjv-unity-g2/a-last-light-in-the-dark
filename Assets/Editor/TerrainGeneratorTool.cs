﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;

public class TerrainGeneratorTool : EditorWindow {

    // Static count used for gameobject name
    static int idCounter = 0;

    // Terrain settings
    int m_terrainScale = 4;

    Vector3 m_sizeFactor = new Vector3(5f, 0.5f, 5f);

    float m_terrainVariation = 1f;

    int m_roughSize = 5;

    float m_maxHeight = 10;

    Material m_terrainMaterial;

    bool roughedEnable = true;

    // Ground required component
    MeshFilter m_terrainMesh;
    MeshRenderer m_terrainMeshRenderer;
    MeshCollider m_terrainGroundCollider;

    // Terrain object settings
    GameObject m_templePrefab;

    GameObject m_ennemiesSpawnPrefab;

    GameObject m_propParent;

    int m_ennmiesSpawnNumber = 20;

    GameObject m_sceneObjectPrefab;

    HeroEntity m_heroEntity;

    GameNpcManager m_npcManager;

    // Window
    [MenuItem("Tool/TerrainGenerator")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(TerrainGeneratorTool));
    }

    void OnGUI()
    {
        GUILayout.Label("Terrain settings", EditorStyles.boldLabel);

        m_terrainScale = EditorGUILayout.IntSlider("Smooth level", m_terrainScale, 1, 6);
        m_terrainVariation = EditorGUILayout.Slider("Variation", m_terrainVariation, 0, 10);
        EditorGUILayout.Separator();

        roughedEnable = EditorGUILayout.BeginToggleGroup("Hard ground leveling", roughedEnable);
            m_roughSize = EditorGUILayout.IntSlider("Level number", m_roughSize, 1, 100);
        EditorGUILayout.EndToggleGroup();

        EditorGUILayout.Separator();
        m_sizeFactor = EditorGUILayout.Vector3Field("Size factor", m_sizeFactor);

        EditorGUILayout.Separator();
        m_maxHeight = EditorGUILayout.FloatField("Max. height", m_maxHeight);

        EditorGUILayout.Separator();
        m_terrainMaterial = (Material) EditorGUILayout.ObjectField("Ground material", m_terrainMaterial, typeof(Material), false);

        GUILayout.Label("Object in map", EditorStyles.boldLabel);
        m_templePrefab = (GameObject) EditorGUILayout.ObjectField("Temple prefab", m_templePrefab, typeof(GameObject), false);

        EditorGUILayout.Separator();
        m_ennmiesSpawnNumber = EditorGUILayout.IntField("Minion spawn", m_ennmiesSpawnNumber);
        m_ennemiesSpawnPrefab = (GameObject) EditorGUILayout.ObjectField("Ennemies prefab", m_ennemiesSpawnPrefab, typeof(GameObject), false);

        EditorGUILayout.Separator();
        m_sceneObjectPrefab = (GameObject)EditorGUILayout.ObjectField("Tree_1", m_sceneObjectPrefab, typeof(GameObject), false);

        EditorGUILayout.Separator();
        m_heroEntity = (HeroEntity) EditorGUILayout.ObjectField("Hero entity", m_heroEntity, typeof(HeroEntity), true);

        EditorGUILayout.Separator();
        m_npcManager = (GameNpcManager)EditorGUILayout.ObjectField("NPC Manager", m_npcManager, typeof(GameNpcManager), true);

        if (GUILayout.Button("Generate Terrain"))
        {
            GenerateTerrain();
        }
    }

    void GenerateTerrain()
    {
        PrepareGroundGameObject();

        //int size_of_map = (1 << ((m_terrainScale < 2) ? 2 : m_terrainScale));

        GenerateGroundMesh();

        // Generate NavMesh
        NavMeshBuilder.BuildNavMesh();
    }

    void PrepareGroundGameObject()
    {
        GameObject ground = new GameObject("Ground");
        ground.isStatic = true;
        ground.transform.position = Vector3.zero;

        m_terrainMesh = ground.AddComponent<MeshFilter>();
        m_terrainMeshRenderer = ground.AddComponent<MeshRenderer>();
        m_terrainGroundCollider = ground.AddComponent<MeshCollider>();

        m_terrainMesh.transform.position = Vector3.zero;
        m_terrainMesh.transform.rotation = Quaternion.identity;

        m_terrainMeshRenderer.material = m_terrainMaterial;

        m_propParent = new GameObject("MapProps");
    }

    int[,] GeneratePropMap()
    {
        int size_of_map = (1 << ((m_terrainScale < 2) ? 2 : m_terrainScale)) + 1;
        int[,] data = new int[size_of_map, size_of_map];

        System.Random rnd = new System.Random();

        // Get random temple position in each map carter
        int temple_pos_x = rnd.Next((size_of_map - 1) / 2);
        int temple_pos_y = rnd.Next((size_of_map - 1) / 2);
        data[temple_pos_x, temple_pos_y] = 1;

        temple_pos_x = rnd.Next((size_of_map - 1) / 2);
        temple_pos_y = rnd.Next((size_of_map - 1) / 2, size_of_map - 1);
        data[temple_pos_x, temple_pos_y] = 1;

        temple_pos_x = rnd.Next((size_of_map - 1) / 2, size_of_map - 1);
        temple_pos_y = rnd.Next((size_of_map - 1) / 2);
        data[temple_pos_x, temple_pos_y] = 1;

        temple_pos_x = rnd.Next((size_of_map - 1) / 2, size_of_map - 1);
        temple_pos_y = rnd.Next((size_of_map - 1) / 2, size_of_map - 1);
        data[temple_pos_x, temple_pos_y] = 1;

        if (m_ennmiesSpawnNumber < size_of_map - 1)
        {
            m_ennmiesSpawnNumber = size_of_map - 1;
        }

        int i = 0;
        while (i < m_ennmiesSpawnNumber)
        {
            int cx = rnd.Next(size_of_map - 1);
            int cy = rnd.Next(size_of_map - 1);

            if (data[cx,cy] == 0)
                data[cx, cy] = 2;

            i++;
        }

        for (int z = 1; z < data.GetLength(0) - 1; z++)
        {
            for (int j = 1; j < data.GetLength(1) - 1; j++)
            {
                if (data[z, j] == 0 && Random.Range(0f, 10f) > 5f)
                {
                    data[z, j] = 3;
                }
            }
        }

        return data;
    }

    void GenerateGroundMesh()
    {
        // Get heightmap generated by DiamanSquare algorithm
        float[,] map = DiamondSquare.generate(m_terrainScale, m_terrainVariation);
        float[] boundary = MinMaxGroundHeight(map);

        // Set materials properties
        m_terrainMeshRenderer.sharedMaterial.SetFloat("_HeightMin", 0);
        m_terrainMeshRenderer.sharedMaterial.SetFloat("_HeightMax", (boundary[0] - boundary[1]) * m_sizeFactor.y);

        // Rough terrain
        if (roughedEnable)
        {
            RoughDataMap(map, m_roughSize, boundary[1], boundary[0]);
        }

        // Create Mesh
        int size_of_map = (1 << ((m_terrainScale < 2) ? 2 : m_terrainScale)) + 1;


        // Get prop
        int[,] propData = GeneratePropMap();
        var m = new Mesh();

        EditorUtility.SetDirty(m_npcManager);
        SerializedObject serializedObject = new UnityEditor.SerializedObject(m_npcManager);
        SerializedProperty serializedNpcList = serializedObject.FindProperty("m_npcPool");

        //serializedNpcList.arraySize = 0;

        Vector3[] vertices = new Vector3[size_of_map * size_of_map];
        for (int i = 0; i < map.GetLength(0); i++)
        {
            for (int j = 0; j < map.GetLength(1); j++)
            {
                float height = (map[i, j] - boundary[1]) / (boundary[0] - boundary[1]) * m_maxHeight;

                // Place props
                if (propData[i, j] == 1)
                {
                    GameObject new_obj = (GameObject) Instantiate(m_templePrefab, new Vector3(i * m_sizeFactor.x, height * m_sizeFactor.y, j * m_sizeFactor.z), Quaternion.identity);
                    new_obj.transform.parent = m_propParent.transform;
                    var capturePointController = (CapturePointController)new_obj.GetComponent<CapturePointController>();
                    capturePointController.Hero = m_heroEntity;
                }
                else if (propData[i, j] == 2)
                {
                    GameObject new_obj = (GameObject) Instantiate(m_ennemiesSpawnPrefab, new Vector3(i * m_sizeFactor.x, height * m_sizeFactor.y, j * m_sizeFactor.z), Quaternion.identity);
                    new_obj.transform.parent = m_propParent.transform;

                    //m_npcManager.InsertNpcAtList((MinionEntity)new_obj.GetComponent<MinionEntity>(), elc);

                    //serializedNpcList.arraySize++;
                    //serializedNpcList.InsertArrayElementAtIndex(elc);
                    //serializedObject.ApplyModifiedProperties();

                    //m_npcManager.InsertNpcAtList((MinionEntity)new_obj.GetComponent<MinionEntity>(), elc);
                    m_npcManager.AddNpcAtList((MinionEntity)new_obj.GetComponent<MinionEntity>());
                }
                else if (propData[i,j] == 3)
                {
                    // Add small randomize for making tree not aligned on a grid
                    GameObject new_obj = (GameObject) Instantiate(m_sceneObjectPrefab, new Vector3(i * m_sizeFactor.x + Random.Range(0, m_sizeFactor.x / 4), height * m_sizeFactor.y, j * m_sizeFactor.z + Random.Range(0, m_sizeFactor.z / 4)), Quaternion.identity);
                    new_obj.transform.parent = m_propParent.transform;
                    new_obj.isStatic = true;
                }

                vertices[size_of_map * i + j] = new Vector3(i * m_sizeFactor.x, height * m_sizeFactor.y, j * m_sizeFactor.z);
            }
        }
        m.vertices = vertices;

        int size_of_mesh = size_of_map - 1;
        int[] triangles = new int[size_of_mesh * size_of_mesh * 6];
        for (int i = 0; i < map.GetLength(0) - 1; i++)
        {
            for (int j = 0; j < map.GetLength(1) - 1; j++)
            {
                int cursor = size_of_map * i + j;
                int cursor_triangle = (size_of_mesh * i + j) * 6;
                triangles[cursor_triangle] = cursor;
                triangles[cursor_triangle + 1] = cursor + 1;
                triangles[cursor_triangle + 2] = cursor + 1 + size_of_map;

                triangles[cursor_triangle + 3] = cursor;
                triangles[cursor_triangle + 4] = cursor + 1 + size_of_map;
                triangles[cursor_triangle + 5] = cursor + size_of_map;
            }
        }

        Vector2[] uvs = new Vector2[vertices.Length];
        for (int i = 0; i < uvs.Length; i++)
        {
            float col = i % size_of_map;
            float line = i / size_of_map;
            uvs[i] = new Vector2(line / size_of_map, col / size_of_map);
        }

        m.uv = uvs;
        m.triangles = triangles;
        m.RecalculateNormals();
        m_terrainMesh.mesh = m;
        m_terrainGroundCollider.sharedMesh = m;
    }

    static int GetGameObjectId()
    {
        idCounter++;
        return idCounter;
    }

    float[] MinMaxGroundHeight(float[,] data_map)
    {
        float[] result = { float.MinValue, float.MaxValue };

        for (int i = 0; i < data_map.GetLength(0); i++)
        {
            for (int j = 0; j < data_map.GetLength(1); j++)
            {
                if (data_map[i, j] > result[0])
                {
                    result[0] = data_map[i, j];
                }
                if (data_map[i, j] < result[1])
                {
                    result[1] = data_map[i, j];
                }
            }
        }

        result[0] = Mathf.Ceil(result[0]);
        result[1] = Mathf.Floor(result[1]);

        return result;
    }

    public void RoughDataMap(float[,] data, int level, float min, float max)
    {
        float levelSize = (max - min) / level;
        for (int i = 0; i < data.GetLength(0); i++)
        {
            for (int j = 0; j < data.GetLength(1); j++)
            {
                data[i, j] = Mathf.Ceil(Mathf.Floor((data[i, j] - min) / levelSize) * levelSize + min);
            }
        }
    }

}
