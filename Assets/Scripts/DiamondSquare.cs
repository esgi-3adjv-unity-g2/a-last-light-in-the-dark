﻿using UnityEngine;
using System.Collections;

public class DiamondSquare {

	public static float[,] generate(int exp, float variation)
    {
        int size = 1 << ((exp < 2) ? 2 : exp);
        float[,] data = new float[size + 1,size + 1];

        // Initialize
        for (int i = 0; i < data.GetLength(0); i++)
        {
            for (int j = 0; j < data.GetLength(1); j++)
            {
                data[i, j] = 0f;
            }
        }

        data[0, 0] = FRand();
        data[0, data.GetLength(1) - 1] = FRand();
        data[data.GetLength(0) - 1, 0] = FRand();
        data[data.GetLength(0) - 1, data.GetLength(1) - 1] = FRand();

        // Run
        int cursor = size;
        float dispalement = 1f;
        while (cursor > 1)
        {
            int mid = cursor / 2;

            // Square step
            for (int x = mid; x < data.GetLength(0); x += cursor)
            {
                for (int y = mid; y < data.GetLength(1); y += cursor)
                {
                    SquareStep(data, x, y, mid, FRand() * mid * dispalement);
                }
            }

            // Diamond step
            for (int x = 0; x < data.GetLength(0); x += mid)
            {
                int shift = (x % cursor == 0) ? mid : 0;
                for (int y = shift; y < data.GetLength(1); y += cursor)
                {
                    DiamondStep(data, x, y, mid, FRand() * mid * dispalement);
                }
            }

            cursor = cursor / 2;
            dispalement = dispalement / variation;
        }

        return data;
    }

    public static void DiamondStep(float[,] data, int x, int y, int mid, float value)
    {
        float sum = 0; int n = 0;

        if (x >= mid)
        {
            sum += data[x - mid,y]; n++;
        }
        if (x + mid < data.GetLength(0))
        {
            sum += data[x + mid,y]; n++;
        }
        if (y >= mid)
        {
            sum += data[x,y - mid]; n++;
        }
        if (y + mid < data.GetLength(1))
        {
            sum += data[x,y + mid]; n++;
        }

        data[x,y] = sum / n + value;
    }

    public static void SquareStep(float[,] data, int x, int y, int size, float value)
    {
        data[x,y] = (data[x - size,y - size] + data[x + size,y + size] + data[x - size,y + size] + data[x + size,y - size]) / 4.0f + value;
    }

    public static float FRand()
    {
        return (Random.value > 0.5) ? Random.value : Random.value * -1;
    }

}
