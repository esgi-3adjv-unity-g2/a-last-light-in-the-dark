﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class GameEntity : NetworkBehaviour {

    [SerializeField]
    Transform m_entityTransform;

    [SerializeField]
    CharacterClass m_character;

    [SerializeField]
    [SyncVar]
    float m_hp;

    [SerializeField]
    List<Skill> m_skills;

    public List<Skill> Skills
    {
        get
        {
            return m_skills;
        }
    }
    
    public Transform EntityTransform
    {
        get
        {
            return m_entityTransform;
        }
    }

    public CharacterClass Character
    {
        get
        {
            return m_character;
        }
    }

    public float HP
    {
        get
        {
            return m_hp;
        }

        set
        {
            m_hp = value;
        }
    }

    public virtual void Start()
    {
        ResetHP();
    }

    public void TakeDamage(float damage)
    {
        if (IsAlive())
        {
            m_hp -= damage;

            if (m_hp < 0)
            {
                m_hp = 0f;
            }
        }
    }

    public bool IsAlive()
    {
        return (m_hp > 0);
    }

    public void ToggleSkill(bool value)
    {
        foreach (Skill s in m_skills)
        {
            s.Enabled = value;
        }
    }

    public void ResetHP()
    {
        m_hp = m_character.DefaultHP;
    }

}
