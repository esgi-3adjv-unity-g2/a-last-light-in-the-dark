﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MinionEntity : GameEntity {

    [SerializeField]
    NavMeshAgent m_navAgent;

    [SerializeField]
    Vector3 m_defaultPosition;

    [SerializeField]
    MinionMoveToTarget m_moveController;

    [SerializeField]
    NetworkIdentity m_netidentity;

    [SerializeField]
    float m_damage;

    float m_initialSpeed;

    public float Damage
    {
        get
        {
            return m_damage;
        }

        set
        {
            m_damage = value;
        }
    }

    public float Speed
    {
        get {
            return m_navAgent.speed;
        }
        set {
            m_navAgent.speed = value;
        }
    }

    public Vector3 DeFaultPosition
    {
        get
        {
            return m_defaultPosition;
        }
    }

    public override void Start()
    {
        base.Start();
        m_initialSpeed = m_navAgent.speed;
        m_defaultPosition = transform.position;
    }

    public void ResetSpeed()
    {
        m_navAgent.speed = m_initialSpeed;
    }

    public void Update()
    {
        if ( ! IsAlive() && m_netidentity.isServer)
        {
            CmdKillMinion();
        }
    }

    public void Spawn()
    {
        m_moveController.ResetTarget();
        transform.position = m_defaultPosition;

        ResetHP();
        gameObject.SetActive(true);
    }

    [Command]
    void CmdKillMinion()
    {
        RpcKillMinion();

        if ( ! Network.isClient)
        {
            transform.position = m_defaultPosition;
            gameObject.SetActive(false);
        }
    }

    [ClientRpc]
    void RpcKillMinion()
    {
        transform.position = m_defaultPosition;
        gameObject.SetActive(false);
    }

}
