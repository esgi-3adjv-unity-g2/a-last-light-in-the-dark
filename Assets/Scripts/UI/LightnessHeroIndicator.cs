﻿using UnityEngine;
using System.Collections;

public class LightnessHeroIndicator : MonoBehaviour {

    [SerializeField]
    GameObject m_arrowIndicatorTop;

    [SerializeField]
    GameObject m_arrowIndicatorBottom;

    [SerializeField]
    GameObject m_arrowIndicatorLeft;

    [SerializeField]
    GameObject m_arrowIndicatorRight;

    [SerializeField]
    Transform m_lightnessPosition;

    [SerializeField]
    Transform m_heroEntityPosition;

    [SerializeField]
    float m_indicatorShowRange;

    bool m_indicatorShow = false;

	void FixedUpdate()
    {
        Vector3 vectorFromPlayer = new Vector3(m_lightnessPosition.position.x - m_heroEntityPosition.position.x, m_lightnessPosition.position.y - m_heroEntityPosition.position.y, m_lightnessPosition.position.z - m_heroEntityPosition.position.z);
        float distanceFromPlayer = Mathf.Sqrt(vectorFromPlayer.x * vectorFromPlayer.x + vectorFromPlayer.z * vectorFromPlayer.z);

        if (distanceFromPlayer > m_indicatorShowRange)
        {
            m_indicatorShow = true;
            if (vectorFromPlayer.x > 0 && vectorFromPlayer.z > 0)
            {
                if (vectorFromPlayer.x > vectorFromPlayer.z)
                {
                    ToggleArrow(2); // Left
                }
                else
                {
                    ToggleArrow(1); // Back
                }
            }
            else
            {
                if (vectorFromPlayer.x > vectorFromPlayer.z)
                {
                    ToggleArrow(0); // Top
                }
                else
                {
                    ToggleArrow(3); // Right
                }
            }
        }
        else
        {
            if (m_indicatorShow)
            {
                m_arrowIndicatorTop.SetActive(false);
                m_arrowIndicatorLeft.SetActive(false);
                m_arrowIndicatorRight.SetActive(false);
                m_arrowIndicatorBottom.SetActive(false);
                m_indicatorShow = false;
            }
        }
    }

    void ToggleArrow(int direction)
    {
        if (direction == 0)
        {
            m_arrowIndicatorTop.SetActive(true);
            m_arrowIndicatorLeft.SetActive(false);
            m_arrowIndicatorRight.SetActive(false);
            m_arrowIndicatorBottom.SetActive(false);
        }
        else if (direction == 1)
        {
            m_arrowIndicatorTop.SetActive(false);
            m_arrowIndicatorLeft.SetActive(false);
            m_arrowIndicatorRight.SetActive(false);
            m_arrowIndicatorBottom.SetActive(true);
        }
        else if (direction == 2)
        {
            m_arrowIndicatorTop.SetActive(false);
            m_arrowIndicatorLeft.SetActive(true);
            m_arrowIndicatorRight.SetActive(false);
            m_arrowIndicatorBottom.SetActive(false);
        }
        else if (direction == 3)
        {
            m_arrowIndicatorTop.SetActive(false);
            m_arrowIndicatorLeft.SetActive(false);
            m_arrowIndicatorRight.SetActive(true);
            m_arrowIndicatorBottom.SetActive(false);
        }
    }

}
