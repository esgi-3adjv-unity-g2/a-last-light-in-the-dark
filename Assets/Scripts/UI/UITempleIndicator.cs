﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UITempleIndicator : MonoBehaviour {

    [SerializeField]
    CapturePointController m_temple;

    [SerializeField]
    Image m_templeIconImage;

    [SerializeField]
    Color m_colorNotCaptured;

    [SerializeField]
    Color m_colorInCapture;

    [SerializeField]
    Color m_colorCaptured;

    bool m_isCaptured;

    bool m_isOnCapture;

	void FixedUpdate()
    {
        //Debug.Log(m_temple.OnCapture);
        if (m_temple)
        {
            if (m_isOnCapture)
            {
                if (m_temple.Captured)
                {
                    m_isOnCapture = false;
                    m_isCaptured = true;
                    m_templeIconImage.color = m_colorCaptured;
                }
                else if ( ! m_temple.OnCapture)
                {
                    m_isOnCapture = false;
                    m_templeIconImage.color = m_colorNotCaptured;
                }
            }
            else if ( ! m_isCaptured)
            {
                if (m_temple.OnCapture)
                {
                    m_isOnCapture = true;
                    m_templeIconImage.color = m_colorInCapture;
                }
            }
        }
    }

}
