﻿using UnityEngine;
using UnityEngine.UI;

public class UISkillbarItem : MonoBehaviour {

    public Image thumbnail;

    public Text textKey;

    public bool skillEnabled = true;

}
