﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class UISkillController : NetworkBehaviour {

    [SerializeField]
    NetworkIdentity m_netIdentity;

    [SerializeField]
    GameEntity m_playerEntity;

    [SerializeField]
    List<UISkillbarItem> m_UISkillItemList;

    [SerializeField]
    PlayerSkillLaunchByInput m_skillInputManager;

	void Start () {

        if (m_netIdentity.isLocalPlayer)
        {
            var skills = m_playerEntity.Skills;

            int skills_n = skills.Count;
            int uiItem_n = m_UISkillItemList.Count;

            for (int i = 0; i < skills_n && i < uiItem_n; i++)
            {
                m_UISkillItemList[i].thumbnail.sprite = skills[i].Thumbnail;
                m_UISkillItemList[i].textKey.text = m_skillInputManager.GetKeyForSkill(i).ToString();
            }
        }

	}
	
	void FixedUpdate () {

        if (m_netIdentity.isLocalPlayer)
        {
            var skills = m_playerEntity.Skills;

            int skills_n = skills.Count;
            int uiItem_n = m_UISkillItemList.Count;

            for (int i = 0; i < skills_n && i < uiItem_n; i++)
            {
                if (m_UISkillItemList[i].skillEnabled && ! skills[i].CanBeUsed())
                {
                    m_UISkillItemList[i].skillEnabled = false;
                    m_UISkillItemList[i].thumbnail.color = new Color(0.5f, 0.5f, 0.5f, 1f);
                }
                else if ( ! m_UISkillItemList[i].skillEnabled && skills[i].CanBeUsed())
                {
                    m_UISkillItemList[i].skillEnabled = true;
                    m_UISkillItemList[i].thumbnail.color = new Color(1f, 1f, 1f, 1f);
                }
            }
        }

	}

}
