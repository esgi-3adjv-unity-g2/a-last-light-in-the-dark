﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIHealthBar : MonoBehaviour {


    [SerializeField]
    HeroEntity hero;

    public Slider healthSlider;
    public float startingHealth;
    private float currentHealth;
    public Image fillImage;
    public Color fullHealthColor = Color.green;
    public Color emptyHealthColor = Color.red;
    public Text hpText;

    public void OnEnable()
    {
        startingHealth = hero.Character.DefaultHP;
    }

    public void FixedUpdate()
    {
        hpText.text = hero.HP.ToString() + " / " + startingHealth.ToString();
        currentHealth = hero.HP;
        SetHealthUi();
    }

    private void SetHealthUi()
    {
        healthSlider.value = currentHealth;
        fillImage.color = Color.Lerp(emptyHealthColor, fullHealthColor, currentHealth / startingHealth);

    }

   
}
