﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIGameEnd : MonoBehaviour
{

    [SerializeField]
    string m_gameEndText;

    [SerializeField]
    string m_winText;

    [SerializeField]
    string m_looseText;

    [SerializeField]
    Text m_textElement;

    [SerializeField]
    protected AudioSource m_winSound;

    [SerializeField]
    protected AudioSource m_loseSound;


    public void DisplayEndUI(bool win)
    {
        m_textElement.text = m_gameEndText + "\n" + ((win) ? m_winText : m_looseText);
        m_textElement.gameObject.SetActive(true);

        if (win)
        {
            m_winSound.Play();
        }
        else
        {
            m_loseSound.Play();
        }
    }

}
