﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIGameAction : MonoBehaviour {

    public void QuitGameAction()
    {
        Application.Quit();
    }

    public void StartHostAction()
    {
        NetworkPlayerAssignManager.Instance.StartHost();
    }

    public void JoinGameAction(InputField address)
    {
        NetworkPlayerAssignManager.Instance.networkAddress = address.text;
        //m_netManager.networkPort = port;
        NetworkPlayerAssignManager.Instance.StartClient();
    }

}
