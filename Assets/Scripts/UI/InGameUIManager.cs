﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class InGameUIManager : MonoBehaviour {

    [SerializeField]
    UIGameEnd m_GUI_GameEnd;

    [SerializeField]
    GameObject m_GUI_PauseMenu;

    [SerializeField]
    KeyCode m_pauseMenuKey;

    [SerializeField]
    GameObject m_networkPrefab;

 


    public void ResumeGame()
    {
        m_GUI_PauseMenu.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(m_pauseMenuKey))
        {
            if (m_GUI_PauseMenu.activeSelf)
            {
                m_GUI_PauseMenu.SetActive(false);
            }
            else
            {
                m_GUI_PauseMenu.SetActive(true);
            }
        }
    }

    public void QuitGame()
    {
        Network.Disconnect(200);
        MasterServer.UnregisterHost();

        SceneManager.LoadScene(0);
        GameObject.Destroy(GameObject.Find("NetworkManager"));
        Instantiate(m_networkPrefab);
    }

    public void EnableGameEndGUI(bool winner)
    {
        var manager = GameRoundManager.Instance;

        m_GUI_GameEnd.gameObject.SetActive(true);

        if (winner)
        {
            m_GUI_GameEnd.DisplayEndUI(manager.HeroPlayer.hasAuthority || manager.LightPlayer.isLocalPlayer);
        }
        else
        {
            m_GUI_GameEnd.DisplayEndUI(manager.DarkPlayer.isLocalPlayer);
        }
    }

}
