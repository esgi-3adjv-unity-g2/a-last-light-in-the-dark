﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using System;

public class MinionMoveToTarget : NetworkBehaviour
{

    [SerializeField]
    NetworkIdentity m_networkIdentity;

    [SerializeField]
    NavMeshAgent m_navAgent;

    [SerializeField]
    Transform m_transform;

    [SerializeField]
    MinionEntity m_entity;

    [SerializeField]
    float m_maxDistanceOfTarget;

    Vector3 m_defaultPosition;

    GameObject m_target;

    List<EntitiesLightScript> m_inLights;

    float m_lastLightHitAt;

    float m_lastHit;

    void Start ()
    {
        m_defaultPosition = transform.position;
        m_inLights        = new List<EntitiesLightScript>();
        m_lastLightHitAt  = Time.time;
        m_lastHit = Time.time;
	}
	
	void Update ()
    {
        if (m_target != null)
        {
            if (m_networkIdentity.hasAuthority && m_inLights.Count == 0 && IsTargetOutOfBound())
            {
                CmdSetTarget(null);
            }
            else
            {
                if (Vector3.Distance(m_target.transform.position, transform.position) >= 3.5f)
                {
                    m_navAgent.SetDestination(m_target.transform.position);
                }
            }
        }
        else
        {
            if (m_networkIdentity.isServer)
            {
                for (int i = 0; i < m_inLights.Count; i++)
                {
                    if (m_inLights[i].Entity == null || !m_inLights[i].IsHeroIn)
                    {
                        CmdRemoveToInLightList(m_inLights[i].gameObject);
                        i -= 1;
                        continue;
                    }
                    if (m_inLights[i].Entity != null && m_inLights[i].IsHeroIn)
                    {
                        CmdSetTarget(m_inLights[i].Entity.gameObject);
                        break;
                    }
                }

                if (m_inLights.Count == 0)
                {
                    CmdSetTarget(null);
                }
            }
        }

        if (m_networkIdentity.isServer && m_inLights.Count > 0 && Time.time - m_lastLightHitAt > 3.0f)
        {
            m_lastLightHitAt = Time.time;
            CmdMinionTakeDamage(1.0f);
        }
    }

    public void ResetTarget()
    {
        m_target = null;
        if (m_inLights != null)
        {
            m_inLights.Clear();
        }
    }

    bool IsTargetOutOfBound()
    {
        float distanceFromTarget = Vector3.Distance(m_target.transform.position, m_transform.position);
        return (distanceFromTarget > m_maxDistanceOfTarget);
    }

    void OnTriggerEnter(Collider other)
    {
        if (m_networkIdentity.isServer && other.CompareTag("EntityLight"))
        {
            CmdAddToInLightList(other.gameObject);
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (m_networkIdentity.isServer && other.CompareTag("HeroPlayer"))
        {
            if (m_lastHit + 3f < Time.time)
            {
                GameRoundManager.Instance.HeroPlayer.Character.TakeDamage(5f);
                m_lastHit = Time.time;
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (m_networkIdentity.isServer && other.CompareTag("EntityLight"))
        {
            CmdRemoveToInLightList(other.gameObject);
        }
    }

    [Command]
    public void CmdMinionTakeDamage(float v)
    {
        RpcMinionTakeDamage(v, m_lastLightHitAt);

        if ( ! Network.isClient)
        {
            m_entity.TakeDamage(v);
        }
    }

    [Command]
    public void CmdSetTarget(GameObject target)
    {
        if (gameObject.activeSelf)
        {
            RpcSetTarget(target);

            if (!Network.isClient)
            {
                m_target = target;

                if (m_target == null)
                {
                    m_navAgent.SetDestination(m_defaultPosition);
                }
            }
        }
    }

    [Command]
    public void CmdAddToInLightList(GameObject lightObject)
    {
        RpcAddToInLightList(lightObject);

        if ( ! Network.isClient)
        {
            m_inLights.Add(lightObject.GetComponent<EntitiesLightScript>());
        }
    }

    [ClientRpc]
    public void RpcAddToInLightList(GameObject lightObject)
    {
        m_inLights.Add(lightObject.GetComponent<EntitiesLightScript>());
    }

    [Command]
    public void CmdRemoveToInLightList(GameObject lightObject)
    {
        RpcRemoveToInLightList(lightObject);

        if (!Network.isClient)
        {
            m_inLights.Remove(lightObject.GetComponent<EntitiesLightScript>());
        }
    }

    [ClientRpc]
    public void RpcRemoveToInLightList(GameObject lightObject)
    {
        m_inLights.Remove(lightObject.GetComponent<EntitiesLightScript>());
    }
    
    [ClientRpc]
    public void RpcSetTarget(GameObject target)
    {
        m_target = target;

        if (m_target == null)
        {
            if (m_navAgent.isActiveAndEnabled)
            {
                m_navAgent.SetDestination(m_defaultPosition);
            }
        }
    }

    [ClientRpc]
    public void RpcMinionTakeDamage(float v, float lastHit)
    {
        m_lastLightHitAt = lastHit;
        m_entity.TakeDamage(v);
    }

}
