﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class HeroMovement : NetworkBehaviour {

    [SerializeField]
    PlayerHeroController m_controller;

    [SerializeField]
    float m_initialMoveSpeed;

    [SerializeField]
    float m_walkDistance;

    [SerializeField]
    Collider mapCollider;

    [SerializeField]
    bool m_freezeMovement;

    [SerializeField]
    Rigidbody m_rigidbody;

    float m_moveSpeed;

    bool isMoving = false;

    public bool FreezeMovement
    {
        get
        {
            return m_freezeMovement;
        }

        set
        {
            m_freezeMovement = value;
        }
    }

    void Start ()
    {
        //GameObject groundObject = GameObject.Find("Ground");
        //mapCollider = groundObject.GetComponent<Collider>();
    }

    void MoveEntity(Vector3 toPosition, Quaternion toRotation, float speed, float walkDistance, bool freeze)
    {
        if ( ! freeze)
        {
            var destinationDistance = Vector3.Distance(toPosition, m_controller.Character.EntityTransform.position);

            // When entity neer destination, it walking instead of running
            m_moveSpeed = (destinationDistance < walkDistance) ? speed / 2 : speed;

            // Go to position
            var playerTransform = m_controller.Character.EntityTransform;
            playerTransform.position = Vector3.MoveTowards(playerTransform.position, toPosition, m_moveSpeed * Time.deltaTime);
            m_rigidbody.velocity = Vector3.zero;
        }
    }

    // Move game object to the position where we click if we hold the click button
    void MoveToPositionFromMousePress()
    {
        if (isLocalPlayer && isMoving)
        {
            Ray ray = m_controller.PlayerCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (mapCollider.Raycast(ray, out hit, 60f))
            {
                Vector3 lookRotation = hit.point - m_controller.Character.EntityTransform.position;
                MoveEntity(hit.point, Quaternion.LookRotation(lookRotation), m_initialMoveSpeed, m_walkDistance, m_freezeMovement);
            }
        }
    }

    void Update ()
    {
        if (isLocalPlayer && ! m_freezeMovement)
        {
            isMoving = Input.GetMouseButton(1);
        }
	}

    void FixedUpdate()
    {
        MoveToPositionFromMousePress();
    }

    [Command]
    public void CmdFreezeMovement(bool value)
    {
        RpcFreezeMovement(value);

        if ( ! Network.isClient)
        {
            m_freezeMovement = value;
        }
    }

    [ClientRpc]
    public void RpcFreezeMovement(bool value)
    {
        m_freezeMovement = value;
    }

}