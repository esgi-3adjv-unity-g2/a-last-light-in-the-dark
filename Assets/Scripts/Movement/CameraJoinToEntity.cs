﻿using UnityEngine;
using System.Collections;

public class CameraJoinToEntity : MonoBehaviour {

    [SerializeField]
    Transform toTransform;

    [SerializeField]
    Transform cameraTransform;

    [SerializeField]
    Camera cameraComponent;

    [SerializeField]
    bool freezeX;

    [SerializeField]
    bool freezeY;

    [SerializeField]
    bool freezeZ;

    public Camera CameraComponent
    {
        get
        {
            return cameraComponent;
        }

        set
        {
            cameraComponent = value;
        }
    }

    public Transform ToTransform
    {
        get
        {
            return toTransform;
        }

        set
        {
            toTransform = value;
        }
    }

    void FixedUpdate() {
        Vector3 newPosition = cameraTransform.position;

	    if (freezeX)
        {
            if (freezeY)
            {
                if ( ! freezeZ)
                {
                    newPosition = new Vector3(cameraTransform.position.x, cameraTransform.position.y, ToTransform.position.z);
                }
            }
            else if (freezeZ)
            {
                newPosition = new Vector3(cameraTransform.position.x, ToTransform.position.y, cameraTransform.position.z);
            }
            else
            {
                newPosition = new Vector3(cameraTransform.position.x, ToTransform.position.y, ToTransform.position.z);
            }
        }
        else
        {
            if (freezeY)
            {
                if (freezeZ)
                {
                    newPosition = new Vector3(ToTransform.position.x, cameraTransform.position.y, cameraTransform.position.z);
                }
                else
                {
                    newPosition = new Vector3(ToTransform.position.x, cameraTransform.position.y, ToTransform.position.z);
                }
            }
            else if (freezeZ)
            {
                newPosition = new Vector3(ToTransform.position.x, ToTransform.position.y, cameraTransform.position.z);
            }
            else
            {
                newPosition = ToTransform.position;
            }
        }

        cameraTransform.position = newPosition;
	}
}
