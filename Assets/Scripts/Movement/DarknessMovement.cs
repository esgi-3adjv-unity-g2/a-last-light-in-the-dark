﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class DarknessMovement : NetworkBehaviour
{

    [SerializeField]
    Transform m_transform;

    [SerializeField]
    float m_speed;

    [SerializeField]
    bool m_freezeMovement;

    bool m_isMovingUp = false;
    bool m_isMovingDown = false;
    bool m_isMovingLeft = false;
    bool m_isMovingRight = false;

    public bool FreezeMovement
    {
        get
        {
            return m_freezeMovement;
        }

        set
        {
            m_freezeMovement = value;
        }
    }

    void Update()
    {
        if (isLocalPlayer && !m_freezeMovement)
        {
            m_isMovingUp = Input.GetKey(KeyCode.UpArrow);
            m_isMovingDown = Input.GetKey(KeyCode.DownArrow);
            m_isMovingLeft = Input.GetKey(KeyCode.LeftArrow);
            m_isMovingRight = Input.GetKey(KeyCode.RightArrow);
        }
    }

    void FixedUpdate()
    {
        if (isLocalPlayer)
        {
            if (m_isMovingUp)
            {
                m_transform.Translate(new Vector3(0, 0, m_speed * Time.deltaTime));
            }
            else if (m_isMovingDown)
            {
                m_transform.Translate(new Vector3(0, 0, -m_speed * Time.deltaTime));
            }

            if (m_isMovingLeft)
            {
                m_transform.Translate(new Vector3(-m_speed * Time.deltaTime, 0, 0));
            }
            else if (m_isMovingRight)
            {
                m_transform.Translate(new Vector3(m_speed * Time.deltaTime, 0, 0));
            }
        }
    }

    [Command]
    public void CmdFreezeMovement(bool value)
    {
        RpcFreezeMovement(value);

        if (!Network.isClient)
        {
            m_freezeMovement = value;
        }
    }

    [ClientRpc]
    public void RpcFreezeMovement(bool value)
    {
        m_freezeMovement = value;
    }

}
