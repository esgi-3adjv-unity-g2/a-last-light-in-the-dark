﻿using UnityEngine;
using System.Collections;

public class EnnemiesGamePool : MonoBehaviour {

    [SerializeField]
    MinionEntity[] m_ennemiesPool;

    public MinionEntity[] EnnemiesPool
    {
        get
        {
            return m_ennemiesPool;
        }

        set
        {
            m_ennemiesPool = value;
        }
    }

}
