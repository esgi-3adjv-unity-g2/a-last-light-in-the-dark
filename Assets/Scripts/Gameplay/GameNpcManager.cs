﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;

public class GameNpcManager : NetworkBehaviour {

    GameRoundManager m_gameManager;

    [SerializeField]
    NetworkIdentity m_netIdentity;

    [SerializeField]
    List<MinionEntity> m_npcPool;

    [SerializeField]
    int m_maxNpcInSameTime;

    // Perform respawn one by one with time interval
    float m_nextNpcRespawn;

    // Use native random for random int generation
    System.Random m_nativeRandom;

    // Game observer -----------------------------

    void Start()
    {
        m_gameManager = GameRoundManager.Instance;
        m_nativeRandom = new System.Random();
        
        if (m_maxNpcInSameTime < 0)
        {
            m_maxNpcInSameTime = 0;
        }

        m_nextNpcRespawn = Time.time;
    }

    void Update()
    {
        if (m_gameManager.GameState == GameRoundManager.GAMESTATE_STARTED && m_netIdentity.isServer && m_nextNpcRespawn < Time.time)
        {
            int numberOfActiveNpc = CountActiveNpc();

            if (numberOfActiveNpc < m_maxNpcInSameTime)
            {
                int tryCount = 0;
                int index = -1;
                float distanceFromPlayer;

                do
                {
                    index = m_nativeRandom.Next(0, m_npcPool.Count);
                    distanceFromPlayer = Vector3.Distance(m_npcPool[index].DeFaultPosition, m_gameManager.HeroPlayer.Character.transform.position);
                    tryCount++;
                } while ((m_npcPool[index].gameObject.activeSelf || distanceFromPlayer < 10f) && tryCount < 5);

                if (index == -1 || tryCount >= 5)
                {
                    m_nextNpcRespawn = Time.time + 5f;
                }
                else
                {
                    CmdSpawnNpc(index);
                    m_nextNpcRespawn = Time.time + 10f;
                }
            }
        }
    }

    public void AddNpcAtList(MinionEntity npc)
    {
        m_npcPool.Add(npc);
    }

    public void InsertNpcAtList(MinionEntity npc, int index)
    {
        m_npcPool[index] = npc;
    }

    public void SpawnNpcAtBeginning()
    {
        if (m_netIdentity.isServer)
        {
            for (int i = m_npcPool.Count; i > m_maxNpcInSameTime; i--)
            {
                int index;
                int tryVar = 0;
                do
                {
                    index = m_nativeRandom.Next(0, m_npcPool.Count);
                    tryVar++;
                } while ( ! m_npcPool[index].gameObject.activeSelf && tryVar < 5);

                CmdDisableNpc(index);
            }
        }
    }

    int CountActiveNpc()
    {
        int result = 0;
        for (int i = 0; i < m_npcPool.Count; i++)
        {
            if (m_npcPool[i].gameObject.activeSelf)
            {
                result++;
            }
        }
        return result;
    }

    // Methods -----------------------------------

    void DisableNpc(int index)
    {
        m_npcPool[index].gameObject.SetActive(false);
    }

    void SpawnNpc(int index)
    {
        m_npcPool[index].Spawn();
    }

    // Command & Rpc -----------------------------

    [Command]
    void CmdDisableNpc(int index)
    {
        RpcDisableNpc(index);

        if ( ! m_netIdentity.isClient)
        {
            DisableNpc(index);
        }
    }

    [ClientRpc]
    void RpcDisableNpc(int index)
    {
        DisableNpc(index);
    }

    [Command]
    void CmdSpawnNpc(int index)
    {
        RpcSpawnNpc(index);

        if ( ! m_netIdentity.isClient)
        {
            SpawnNpc(index);
        }
    }

    [ClientRpc]
    void RpcSpawnNpc(int index)
    {
        SpawnNpc(index);
    }

}
