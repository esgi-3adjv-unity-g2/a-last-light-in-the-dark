﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class CapturePointController : NetworkBehaviour {

    [SerializeField]
    NetworkIdentity m_netidentity;

    [SerializeField]
    EntitiesLightScript m_light;

    [SerializeField]
    HeroEntity m_hero;

    [SerializeField]
    float m_captureZoneSize;

    [SerializeField]
    float m_captureSpeed;

    [SerializeField]
    AudioSource m_captureSound;

    float m_initialZoneRadius;

    bool m_isCaptured;

    bool m_isOnCapture;

    public bool OnCapture
    {
        get
        {
            return m_isOnCapture;
        }
    }

    public bool Captured
    {
        get
        {
            return m_isCaptured;
        }
    }

    public HeroEntity Hero
    {
        get
        {
            return m_hero;
        }

        set
        {
            m_hero = value;
        }
    }

	void Start ()
    {
        m_initialZoneRadius = m_light.GetSize();
        m_isCaptured = false;
        m_isOnCapture = false;

        if (GameRoundManager.Instance != null)
        {
            GameRoundManager.Instance.AddCapturePoint(this);
        }
	}

    void FixedUpdate()
    {
        if ( ! m_isCaptured)
        {
            if (m_isOnCapture)
            {
                m_light.SetSize(m_light.GetSize() + Time.deltaTime * m_captureSpeed);
            }
            else if (m_light.GetSize() > m_initialZoneRadius)
            {
                m_light.SetSize(m_light.GetSize() - Time.deltaTime * m_captureSpeed);
            }
        }
    }

    void Update()
    {
        if (m_netidentity.isServer && ! m_isCaptured && m_light.GetSize() >= m_captureZoneSize)
        {
            CmdSetZoneCaptured();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (m_netidentity.isServer && ! m_isCaptured && other.gameObject.CompareTag("HeroPlayer"))
        {
            CmdSetOnCapture();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (m_netidentity.isServer && other.gameObject.CompareTag("HeroPlayer"))
        {
            CmdRemoveOnCapture();
        }
    }

    [Command]
    public void CmdSetOnCapture()
    {
        RpcSetOnCapture();

        if ( ! Network.isClient)
        {
            m_hero.ToggleSkill(false);
            m_isOnCapture = true;
        }
    }

    [ClientRpc]
    public void RpcSetOnCapture()
    {
        m_hero.ToggleSkill(false);
        m_isOnCapture = true;
    }

    [Command]
    public void CmdRemoveOnCapture()
    {
        RpcRemoveOnCapture();

        if ( ! Network.isClient)
        {
            m_hero.ToggleSkill(true);
            m_isOnCapture = false;
        }
    }

    [ClientRpc]
    public void RpcRemoveOnCapture()
    {
        m_hero.ToggleSkill(true);
        m_isOnCapture = false;
    }

    [Command]
    public void CmdSetZoneCaptured()
    {
        RpcSetZoneCaptured();

        if ( ! Network.isClient)
        {
            m_isCaptured = true;
            m_light.SetSize(m_captureZoneSize);
        }
    }

    [ClientRpc]
    public void RpcSetZoneCaptured()
    {
        m_isCaptured = true;
        m_light.SetSize(m_captureZoneSize);
        m_captureSound.Play();
    }

}
