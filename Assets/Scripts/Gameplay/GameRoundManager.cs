﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class GameRoundManager : NetworkBehaviour {

    public const int GAMESTATE_STARTED = 0;
    public const int GAMESTATE_END = 1;
    public const int GAMESTATE_BEGIN = -1;

    #region UnityCompliant Singleton
    public static GameRoundManager Instance
    {
        get;
        private set;
    }

    public virtual void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            return;
        }

        Destroy(this.gameObject);
    }
    #endregion

    [SerializeField]
    NetworkIdentity m_netIdentity;

    [SerializeField]
    PlayerHeroController m_heroPlayer;

    [SerializeField]
    PlayerLightController m_lightPlayer;

    [SerializeField]
    PlayerDarknessController m_darkPlayer;

    [SerializeField]
    InGameUIManager m_guiManager;

    [SerializeField]
    List<CapturePointController> m_capturePoints;

    [SerializeField]
    GameNpcManager m_npcManager;

    bool m_winner;

    // Game state
    bool m_isGameEnd;

    bool m_isGameRun;

    public PlayerHeroController HeroPlayer
    {
        get
        {
            return m_heroPlayer;
        }
    }

    public PlayerLightController LightPlayer
    {
        get
        {
            return m_lightPlayer;
        }
    }

    public PlayerDarknessController DarkPlayer
    {
        get
        {
            return m_darkPlayer;
        }
    }

    public int GameState
    {
        get
        {
            return (m_isGameRun) ? GAMESTATE_STARTED : ((m_isGameEnd) ? GAMESTATE_END : GAMESTATE_BEGIN);
        }
    }

    // Game observer -----------------------------

    void Start () {
        // Init game state
        m_isGameRun = false;
        m_isGameEnd = false;

        // Freeze player
        /*if ( ! m_isGameRun || m_isGameEnd)
        {
            m_heroPlayer.PlayerMovement.FreezeMovement = true;
            m_lightPlayer.PlayerMovement.FreezeMovement = true;
            m_darkPlayer.PlayerMovement.FreezeMovement = true;
        }*/
        //m_npcManager.SpawnNpcAtBeginning();
    }
	
	void FixedUpdate () {
        if (m_netIdentity.isServer)
        {
            if (GameState == GAMESTATE_BEGIN)
            {
                StartGame();
            }
            else if (GameState == GAMESTATE_STARTED)
            {
                UpdateGame();
            }
        }
	}

    void StartGame()
    {
        if (m_netIdentity.isServer && ! ControllerPlayerGamePool.Instance.HasAvailableController())
        {
            // Spawn NPC
            m_npcManager.SpawnNpcAtBeginning();

            // Un-freeze player
            m_heroPlayer.PlayerMovement.CmdFreezeMovement(false);
            m_lightPlayer.PlayerMovement.CmdFreezeMovement(false);
            m_darkPlayer.PlayerMovement.CmdFreezeMovement(false);

            // Start game
            CmdStartGame();
        }
    }

    void UpdateGame()
    {
        // End Game
        if (IsAllPointCaptured())
        {
            CmdEndGame(true);
        }
        else if ( ! m_heroPlayer.Character.IsAlive())
        {
            CmdEndGame(false);
        }
    }

    bool IsAllPointCaptured()
    {
        bool result = true;
        for (int i = 0; i < m_capturePoints.Count; i++)
        {
            if ( ! m_capturePoints[i].Captured)
            {
                result = false;
                break;
            }
        }

        return result;
    }

    public void AddCapturePoint(CapturePointController cp)
    {
        if ( ! m_capturePoints.Contains(cp))
        {
            m_capturePoints.Add(cp);
        }
    }

    // Command & Rpc -----------------------------

    [Command]
    void CmdStartGame()
    {
        RpcStartGame();

        if ( ! Network.isClient)
        {
            m_isGameRun = true;
        }
    }

    [ClientRpc]
    void RpcStartGame()
    {
        m_isGameRun = true;
    }

    [Command]
    void CmdEndGame(bool winner)
    {
        m_winner = winner;
        m_heroPlayer.PlayerMovement.CmdFreezeMovement(true);
        m_lightPlayer.PlayerMovement.CmdFreezeMovement(true);
        m_darkPlayer.PlayerMovement.CmdFreezeMovement(true);

        RpcEndGame(winner);

        if ( ! Network.isClient)
        {
            m_winner = winner;
            m_isGameEnd = true;
            m_isGameRun = false;
        }
    }

    [ClientRpc]
    void RpcEndGame(bool winner)
    {
        m_winner = winner;

        m_isGameEnd = true;
        m_isGameRun = false;

        m_guiManager.EnableGameEndGUI(winner);
    }

}
