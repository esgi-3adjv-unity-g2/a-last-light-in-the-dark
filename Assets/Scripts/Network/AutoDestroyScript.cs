﻿using UnityEngine;
using System.Collections;

public class AutoDestroyScript : MonoBehaviour
{

    public void Awake()
    {
        Destroy(this.gameObject);
    }

}