﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class ControllerPlayerGamePool : NetworkBehaviour {

    #region UnityCompliant Singleton
    public static ControllerPlayerGamePool Instance
    {
        get;
        private set;
    }

    public virtual void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            return;
        }

        Destroy(this.gameObject);
    }
    #endregion

    [SerializeField]
    GameObject[] controllers;

    private Queue<GameObject> availableControllers = null;

    private Queue<GameObject> AvailableControllers
    {
        get
        {
            if (availableControllers == null)
            {
                availableControllers = new Queue<GameObject>(controllers);
            }

            return availableControllers;
        }
    }

    public bool HasAvailableController()
    {
        return (AvailableControllers.Count > 0);
    }

    public GameObject GetNextAvailableController()
    {
        if (HasAvailableController())
        {
            return AvailableControllers.Dequeue();
        }

        return null;
    }

    public void ReleaseController(GameObject controller)
    {
        if (controller == null)
        {
            return;
        }

        AvailableControllers.Enqueue(controller);
    }

}
