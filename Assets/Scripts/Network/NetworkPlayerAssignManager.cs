﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class NetworkPlayerAssignManager : NetworkManager {

    #region UnityCompliant Singleton
    public static NetworkPlayerAssignManager Instance
    {
        get;
        private set;
    }

    public virtual void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            return;
        }

        Destroy(this.gameObject);
    }
    #endregion

    Dictionary<NetworkConnection, GameObject> activeControllers = null;

    public Dictionary<NetworkConnection, GameObject> ActiveControllers
    {
        get
        {
            if (activeControllers == null)
            {
                activeControllers = new Dictionary<NetworkConnection, GameObject>();
            }

            return activeControllers;
        }
    }

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
        base.OnServerAddPlayer(conn, playerControllerId);

        var player = ControllerPlayerGamePool.Instance.GetNextAvailableController();
        if (player)
        {
            ActiveControllers.Add(conn, player);
            NetworkServer.ReplacePlayerForConnection(conn, player, playerControllerId);
        }
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        if (activeControllers.ContainsKey(conn))
        {
            conn.playerControllers.Clear();
            ControllerPlayerGamePool.Instance.ReleaseController(activeControllers[conn]);
            ActiveControllers.Remove(conn);
        }

        base.OnServerDisconnect(conn);
    }

}
