﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class JugmentHammerSkill : Skill
{

    [SerializeField]
    GameObject m_prefab;

    [SerializeField]
    Collider m_mapCollider;

    [SerializeField]
    PlayerHeroController m_playerController;

    [SerializeField]
    int damage;

    public JugmentHammerSkill() : base() { }

    public override void Launch()
    {
        if (m_netIdentity.hasAuthority && m_mapCollider != null && CanBeUsed())
        {
            m_canBeUsedAt = Time.time + m_countdown;

            if (m_netIdentity.hasAuthority)
            {
                Ray ray = m_playerController.PlayerCamera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (m_mapCollider.Raycast(ray, out hit, 60f))
                {
                    CmdLaunchSkill(hit.point);
                    if (m_sound != null)
                    {
                        m_sound.Play();
                    }
                }
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Minion"))
        {
            MinionEntity target = (MinionEntity)other.GetComponent<MinionEntity>();
            target.TakeDamage(damage);
        }
    }

    [Command]
    public void CmdLaunchSkill(Vector3 point)
    {
        var beacon = (GameObject)Instantiate(m_prefab, point, Quaternion.identity);
        NetworkServer.Spawn(beacon);
    }

}
