﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class BeaconSupportDamage : Skill {

    [SerializeField]
    GameObject m_prefab;

    [SerializeField]
    Collider m_mapCollider;

    [SerializeField]
    PlayerLightController m_playerController;

    public BeaconSupportDamage() : base() { }

    public override void Launch()
    {
        if (m_mapCollider != null && CanBeUsed())
        {
            m_canBeUsedAt = Time.time + m_countdown;

            if (true)
            {
                Ray ray = m_playerController.PlayerCamera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (m_mapCollider.Raycast(ray, out hit, 60f))
                {
                    CmdLaunchSkill(hit.point);
                }
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Minion"))
        {
            MinionEntity target = (MinionEntity)other.GetComponent<MinionEntity>();
            target.Damage = target.Damage * 0.5f;
        }
    }

    [Command]
    public void CmdLaunchSkill(Vector3 point)
    {
        if (m_netIdentity.isServer && m_netIdentity.hasAuthority)
        {
            var beacon = (GameObject)Instantiate(m_prefab, point, Quaternion.identity);
            if (m_sound != null)
            {
                m_sound.Play();
            }
            NetworkServer.Spawn(beacon);
        }
    }
}
