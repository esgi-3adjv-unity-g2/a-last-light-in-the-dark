﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MinionInvokBossSkill : Skill
{

    [SerializeField]
    GameObject m_prefab;

    [SerializeField]
    Collider m_mapCollider;

    [SerializeField]
    PlayerDarknessController m_playerController;

    public MinionInvokBossSkill() : base() { }

    public override void Launch()
    {
        if (m_netIdentity.hasAuthority && m_mapCollider != null && CanBeUsed())
        {
            m_canBeUsedAt = Time.time + m_countdown;

            if (m_netIdentity.hasAuthority)
            {
                Ray ray = m_playerController.PlayerCamera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (m_mapCollider.Raycast(ray, out hit, 60f))
                {
                    CmdLaunchSkill(hit.point);
                    if (m_sound != null)
                    {
                        m_sound.Play();
                    }
                }
            }
        }
    }

    [Command]
    public void CmdLaunchSkill(Vector3 point)
    {
            var MinionInvok = (GameObject)Instantiate(m_prefab, point, Quaternion.identity);

            NetworkServer.Spawn(MinionInvok);
    }
}
