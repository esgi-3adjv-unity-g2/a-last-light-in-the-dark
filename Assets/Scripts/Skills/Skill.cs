﻿using UnityEngine;
using UnityEngine.Networking;

public abstract class Skill : NetworkBehaviour {

    [SerializeField]
    protected NetworkIdentity m_netIdentity;

    [SerializeField]
    protected string m_name;

    [SerializeField]
    protected string m_description;

    [SerializeField]
    protected float m_defaultCountdown;

    [SerializeField]
    protected float m_countdown;

    [SerializeField]
    protected bool m_enabled;

    [SerializeField]
    protected Sprite m_thumbnail;

    [SerializeField]
    protected AudioSource m_sound;

    // Next time than player can re-use skill
    // see http://docs.unity3d.com/ScriptReference/Time-time.html
    protected float m_canBeUsedAt;

    public bool Enabled
    {
        get
        {
            return m_enabled;
        }
        set
        {
            m_enabled = value;
        }
    }

    public string Name
    {
        get
        {
            return m_name;
        }
    }

    public string Description
    {
        get
        {
            return m_description;
        }
    }

    public float DefaultCountdown
    {
        get
        {
            return m_defaultCountdown;
        }
    }

    public float Countdown
    {
        get
        {
            return m_countdown;
        }
        set
        {
            m_countdown = value;
        }
    }

    public float CanBeUsedAt
    {
        get
        {
            return m_canBeUsedAt;
        }
        set
        {
            m_canBeUsedAt = value;
        }
    }

    public Sprite Thumbnail
    {
        get {
            return m_thumbnail;
        }
    }

    public virtual void Start()
    {
        m_countdown = m_defaultCountdown;
    }

    /* # Launch
     * It's abstract method defined in class that herited of Skill class. Is here
     * that we place skill effect and it's the method that we need to call when we 
     * want to launch skill.
     */
    public abstract void Launch();

    /* # GetTimer
     * Return time in float before skill can be re-used.
     */
    public float GetTimer()
    {
        return m_canBeUsedAt - Time.time;
    }

    /* # CanBeUsed
     * Return boolean if skill can be used (check timer)
     */
    public bool CanBeUsed()
    {
        return (m_enabled && GetTimer() <= 0);
    }

    /* # ResetNextUse
     * Reset waiting time before next uses of skill
     */
    public void ResetNextUse()
    {
        m_canBeUsedAt = Time.time;
    }

}
