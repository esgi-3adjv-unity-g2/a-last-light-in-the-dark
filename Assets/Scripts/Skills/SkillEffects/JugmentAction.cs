﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class JugmentAction : NetworkBehaviour
{

    [SerializeField]
    NetworkIdentity m_netIdentity;


    [SerializeField]
    float m_duration;

    float m_expireTime;

    float m_decreasePerSecond;

    void Start()
    {
        m_netIdentity = GetComponent<NetworkIdentity>();

        m_expireTime = Time.time + m_duration;
    }

    void Update()
    {

        if (m_netIdentity.hasAuthority && m_expireTime < Time.time)
        {
            NetworkServer.Destroy(this.gameObject);
        }
    }
}
