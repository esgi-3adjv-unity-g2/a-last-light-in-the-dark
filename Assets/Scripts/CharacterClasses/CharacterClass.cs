﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterClass : MonoBehaviour {

    [SerializeField]
    string m_characterName;

    [SerializeField]
    Sprite m_thumbnail;

    [SerializeField]
    float m_defaultHP;

    [SerializeField]
    Skill[] m_skills;

    public float DefaultHP
    {
        get {
            return m_defaultHP;
        }
    }

    public Skill[] Skills
    {
        get {
            return m_skills;
        }
    }

    public Sprite Thumbnail
    {
        get {
            return m_thumbnail;
        }
    }

    public string CharacterName
    {
        get {
            return m_characterName;
        }
    }

}
