﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerDarknessController : NetworkBehaviour
{

    [SerializeField]
    GameObject m_mapLight;

    [SerializeField]
    PlayerInformation m_playerInformation;

    [SerializeField]
    Camera m_playerCamera;

    [SerializeField]
    DarknessMovement m_playerMovement;

    [SerializeField]
    GameObject m_heroHealthBar;

    public DarknessMovement PlayerMovement
    {
        get
        {
            return m_playerMovement;
        }
    }

    public Camera PlayerCamera
    {
        get
        {
            return m_playerCamera;
        }
    }

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
        if (Camera.main && Camera.main.gameObject)
        {
            Camera.main.gameObject.SetActive(false);
        }

        m_playerCamera.gameObject.SetActive(true);
        m_mapLight.SetActive(true);
        m_heroHealthBar.SetActive(true);
    }

}
