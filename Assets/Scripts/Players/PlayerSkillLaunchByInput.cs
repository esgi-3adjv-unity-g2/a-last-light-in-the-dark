﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class PlayerSkillLaunchByInput : NetworkBehaviour {

    [SerializeField]
    GameEntity m_entity;

    [SerializeField]
    NetworkIdentity m_netIdentity;

    [SerializeField]
    KeyCode m_launchSkillKey_1;

    [SerializeField]
    KeyCode m_launchSkillKey_2;

    [SerializeField]
    KeyCode m_launchSkillKey_3;

    [SerializeField]
    KeyCode m_launchSkillKey_4;

    Queue<int> m_skillToCast;

	void Start () {
        m_skillToCast = new Queue<int>();
	}
	
	void Update () {
        int skillCount = m_entity.Skills.Count;

        if (Input.GetKeyDown(m_launchSkillKey_1) && skillCount > 0)
        {
            AddSkillToQueue(0);
        }
        if (Input.GetKeyDown(m_launchSkillKey_2) && skillCount > 1)
        {
            AddSkillToQueue(1);
        }
        if (Input.GetKeyDown(m_launchSkillKey_3) && skillCount > 2)
        {
            AddSkillToQueue(2);
        }
        if (Input.GetKeyDown(m_launchSkillKey_4) && skillCount > 3)
        {
            AddSkillToQueue(3);
        }
    }

    void AddSkillToQueue(int s)
    {
        if ( ! m_skillToCast.Contains(s))
        {
            m_skillToCast.Enqueue(s);
        }
    }

    void FixedUpdate()
    {
        if (m_netIdentity && m_netIdentity.isLocalPlayer && m_skillToCast.Count > 0)
        {
            CmdLaunchPlayerSkill(m_skillToCast.Dequeue());
        }
    }

    public KeyCode GetKeyForSkill(int index)
    {
        KeyCode r;

        switch (index)
        {
            case 0:
                r = m_launchSkillKey_1;
                break;
            case 1:
                r = m_launchSkillKey_2;
                break;
            case 2:
                r = m_launchSkillKey_3;
                break;
            case 3:
                r = m_launchSkillKey_4;
                break;
            default:
                r = KeyCode.None;
                break;
        }

        return r;
    }

    [Command]
    public void CmdLaunchPlayerSkill(int skillNumber)
    {
        RpcLaunchPlayerSkill(skillNumber);

        /*if ( ! Network.isClient)
        {
            if (skillNumber < m_entity.Skills.Count)
            {
                if (m_entity.Skills[skillNumber].CanBeUsed())
                    m_entity.Skills[skillNumber].Launch();
            }
        }*/
    }

    [ClientRpc]
    public void RpcLaunchPlayerSkill(int skillNumber)
    {
        if (skillNumber < m_entity.Skills.Count && m_netIdentity.isLocalPlayer)
        {
            if (m_entity.Skills[skillNumber].CanBeUsed())
                m_entity.Skills[skillNumber].Launch();
        }
    }

}
