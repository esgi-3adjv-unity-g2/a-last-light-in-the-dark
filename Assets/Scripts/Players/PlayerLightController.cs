﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerLightController : NetworkBehaviour {

    [SerializeField]
    PlayerInformation m_playerInformation;

    [SerializeField]
    Camera m_playerCamera;

    [SerializeField]
    LightMovement m_playerMovement;

    [SerializeField]
    GameObject m_heroHealthBar;

    [SerializeField]
    LightnessHeroIndicator m_heroIndicator;

    public LightMovement PlayerMovement
    {
        get
        {
            return m_playerMovement;
        }
    }

    public Camera PlayerCamera
    {
        get
        {
            return m_playerCamera;
        }
    }

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
        if (Camera.main && Camera.main.gameObject)
        {
            Camera.main.gameObject.SetActive(false);
        }

        m_playerCamera.gameObject.SetActive(true);

        m_heroHealthBar.SetActive(true);
        m_heroIndicator.gameObject.SetActive(true);

    }

}
