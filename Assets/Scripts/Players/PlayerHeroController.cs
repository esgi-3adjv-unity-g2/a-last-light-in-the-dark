﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerHeroController : NetworkBehaviour {

    [SerializeField]
    HeroEntity m_character;

    [SerializeField]
    PlayerInformation m_playerInformation;

    [SerializeField]
    Camera m_playerCamera;

    [SerializeField]
    HeroMovement m_playerMovement;

    [SerializeField]
    GameObject m_heroHealthBar;


    public HeroMovement PlayerMovement
    {
        get
        {
            return m_playerMovement;
        }
    }

    public HeroEntity Character
    {
        get
        {
            return m_character;
        }
    }

    public Camera PlayerCamera
    {
        get
        {
            return m_playerCamera;
        }
    }

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
        if (Camera.main && Camera.main.gameObject)
        {
            Camera.main.gameObject.SetActive(false);
        }

        m_playerCamera.gameObject.SetActive(true);
        m_heroHealthBar.SetActive(true);
    }

}
