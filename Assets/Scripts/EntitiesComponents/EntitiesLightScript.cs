﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class EntitiesLightScript : NetworkBehaviour {

    [SerializeField]
    NetworkIdentity m_networkIdentity;

    [SerializeField]
    HeroEntity m_hero;

    [SerializeField]
    Light m_light;

    [SerializeField]
    BoxCollider m_collider;

    [SyncVar]
    bool m_isHeroIn;

    public HeroEntity Entity
    {
        get
        {
            return m_hero;
        }
    }

    public bool IsHeroIn
    {
        get
        {
            return m_isHeroIn;
        }
    }

    public Light Light
    {
        get
        {
            return m_light;
        }
    }

    void Start()
    {
        m_hero = GameRoundManager.Instance.HeroPlayer.Character;
    }

    public void SetSize(float value)
    {
        m_collider.size = new Vector3(value, m_collider.size.y, value);
        m_light.range = value;
    }

    public float GetSize()
    {
        return m_collider.size.x;
    }

    void OnTriggerEnter(Collider other)
    {
        if (m_networkIdentity.isServer && other.gameObject.CompareTag("HeroPlayer")) {
            m_isHeroIn = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (m_networkIdentity.isServer && other.gameObject.CompareTag("HeroPlayer"))
        {
            m_isHeroIn = false;
        }
    }

}
