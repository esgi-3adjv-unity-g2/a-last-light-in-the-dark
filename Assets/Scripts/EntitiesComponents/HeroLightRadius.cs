﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class HeroLightRadius : NetworkBehaviour {

    [SerializeField]
    GameEntity m_heroEntity;

    [SerializeField]
    Light m_heroLight;

    [Range(0, 10)]
    [SerializeField]
    float m_minLightRange;

    [Range(0, 10)]
    [SerializeField]
    float m_maxLightRange;

    void FixedUpdate()
    {
        float lightRange = (m_heroEntity.HP / m_heroEntity.Character.DefaultHP) * (m_maxLightRange - m_minLightRange) + m_minLightRange;
        m_heroLight.range = lightRange;
    }

}
